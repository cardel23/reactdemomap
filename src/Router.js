import React, { Component, Fragment } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
// import SeccionPruebas from './components/SeccionPruebas';
// import Componente1 from './components/Componente1';
import Error404 from './components/Error';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
// import Blog from './components/Blog';
// import Formulario from './components/Formulario';
import Sidebar from './components/Sidebar';
// import Peliculas from './components/Peliculas';
// import Search from './components/Search';
// import Article from './components/Article';
// import CreateArticle from './components/CreateArticle';
// import EditArticle from './components/EditArticle';


// Rutas
class Router extends Component {


    render() {

        return (
            // 
            <BrowserRouter>

                {/* Hay que cargar los enlaces de navlink dentro del router*/}
                <Header />



                {/* AQUI VAN LAS RUTAS */}

                <Switch>
                    {/* <Route exact path='/ruta-prueba' component={SeccionPruebas} />
                    <Route exact path='/segunda-ruta-prueba' component={Componente1} /> */}
                    <Route exact path='/' component={Home} />
                    <Route exact path='/home' component={Home} />
                    {/* <Route exact path='/blog' component={Blog} />
                    <Route exact path='/formulario' component={Formulario} />
                    <Route exact path='/peliculas' component={Peliculas} />
                    <Route exact path='/search/:search' component={Search} />
                    <Route exact path='/blog/article/:id' component={Article} />
                    <Route exact path='/blog/article/edit/:id' component={EditArticle} />
                    <Route exact path='/blog/new' component={CreateArticle} />
                    <Route exact path='/redirect/:search' render={
                        (props) =>{
                            var search = props.match.params.search;
                            return(<Redirect to={"/search/" + search}/>)
                        }
                    } /> */}

                    {/* Renderear directo */}
                    {/* <Route exact path="/pagina" render={() => (
                        <Fragment>
                            <div id='main'>
                            <h1>Ola k ase</h1>
                            <Componente1 saludo="Como esta o k ase mira la receta o k ase" />
                            </div>
                        </Fragment>
                    )} /> */}

                    {/* Simular un componente  */}
                    {/* <Route exact path="/pruebas/:id" render={(props) => {

                        var id = props.match.params.id;
                        return (
                            <Fragment>
                                <section id="content">
                                    <h1 className="subheader">Pagina de pruebas</h1>
                                    <h2>{id}</h2>
                                </section>

                                <Sidebar />

                                <div className="clearfix"></div>
                            </Fragment>
                        );
                    }} /> */}

                    {/* <Route exact path="/prueba/:nombre/:apellido?" render={(props) => {

                        var nombre = props.match.params.nombre;
                        var apellido = props.match.params.apellido;
                        return (
                            <Fragment>
                                <section id="content">
                                    <h1 className="subheader">Pagina de pruebas</h1>
                                    {
                                        <h2>
                                            {
                                                nombre && !apellido &&
                                                <span>Hola soy {nombre}</span>
                                            }

                                            {
                                                nombre && apellido &&
                                                <span>Hola soy {nombre} {apellido}</span>
                                            }
                                        </h2>
                                    }

                                </section>
                                <Sidebar />

                                <div className="clearfix"></div>
                            </Fragment>
                        );
                    }} /> */}

                    {/* ESTO TIENE QUE IR DE ULTIMO SI NO NO RUTEA */}
                    <Route component={Error404} />
                </Switch>




                <Footer />

            </BrowserRouter >
        );
    }
}

export default Router;