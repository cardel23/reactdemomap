import React, { Component } from "react";
import Sidebar from "./Sidebar";
import mapboxgl from 'mapbox-gl';
import circle from "../assets/images/point.png"

import $ from 'jquery';

mapboxgl.accessToken = 'pk.eyJ1IjoiY2FyZGVsOTEiLCJhIjoiY2tiZDJkajl6MDdydTJxcWVyZG43dmJzZiJ9.4OadVjeSz2M0TD1JWrt-6A';

class Home extends Component {
    currentMarker = {
        lng: "",
        lat: "",
        date: null
    };

    markers = [];
    map = {};
    constructor(props) {
        super(props);
        this.state = {
            lng: -86.2607,
            lat: 12.1203,
            zoom: 16,
            coords: [
                // [-86.2585, 12.1212],
                // [-86.2408, 12.0949]
            ]
        };
        
    }

    draw = {};
    
    // Add the draw tool to the map

    componentDidMount() {
        
        this.map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom
        });

        this.map.on('load', ()=> {
            this.map.resize();
            this.map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': this.state.coords
                    }
                }
            });
            this.map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#888',
                    'line-width': 8
                }
            });
        });
    
        this.map.on('move', () => {
            this.setState({
                lng: this.map.getCenter().lng.toFixed(4),
                lat: this.map.getCenter().lat.toFixed(4),
                zoom: this.map.getZoom().toFixed(2)
            });
            this.currentMarker = {
                lng: this.map.getCenter().lng.toFixed(4),
                lat: this.map.getCenter().lat.toFixed(4),
                date: new Date().getUTCDate()
            };
        });
        this.map.addControl(new mapboxgl.NavigationControl());
        // this.draw = new MapboxDraw({
        //     // Instead of showing all the draw tools, show only the line string and delete tools
        //         displayControlsDefault: true,
        //         // controls: {
        //         //     // line_string: true,
        //         //     // trash: true
        //         // },
        //         styles: [
        //             // Set the line style for the user-input coordinates
        //             {
        //             "id": "gl-draw-line",
        //             "type": "line",
        //             "filter": ["all", ["==", "$type", "LineString"],
        //                 ["!=", "mode", "static"]
        //             ],
        //             "layout": {
        //                 "line-cap": "round",
        //                 "line-join": "round"
        //             },
        //             "paint": {
        //                 "line-color": "#438EE4",
        //                 "line-dasharray": [0.2, 2],
        //                 "line-width": 4,
        //                 "line-opacity": 0.7
        //             }
        //             },
        //             // Style the vertex point halos
        //             {
        //             "id": "gl-draw-polygon-and-line-vertex-halo-active",
        //             "type": "circle",
        //             "filter": ["all", ["==", "meta", "vertex"],
        //                 ["==", "$type", "Point"],
        //                 ["!=", "mode", "static"]
        //             ],
        //             "paint": {
        //                 "circle-radius": 12,
        //                 "circle-color": "#FFF"
        //             }
        //             },
        //             // Style the vertex points
        //             {
        //             "id": "gl-draw-polygon-and-line-vertex-active",
        //             "type": "circle",
        //             "filter": ["all", ["==", "meta", "vertex"],
        //                 ["==", "$type", "Point"],
        //                 ["!=", "mode", "static"]
        //             ],
        //             "paint": {
        //                 "circle-radius": 8,
        //                 "circle-color": "#438EE4",
        //             }
        //             },
        //         ]
        //     });

        // this.map.addControl(this.draw);

        // this.map.on('draw.create', this.updateRoute);
        // this.map.on('draw.update', this.updateRoute);
    }

    newMarker = (point) => {
        
        console.log(point);
        // this.state.markers.push(marker);
        this.state.coords.push([Number(point.lng), Number(point.lat)]);
        var popup = new mapboxgl.Popup({ offset: [0, -15] }).setText(
            point.date
            )
            // .setLngLat([marker.lng, marker.lat])
            // .addTo(this.map)
            ;

        var el = document.createElement('div');
        el.id = "marker"+this.state.coords.length;
        el.className = 'marker';
        el.style.backgroundImage =`url(${circle})`;
        var marker = new mapboxgl.Marker(el)
            .setLngLat([point.lng, point.lat])
            .setPopup(popup)
            // .setHTML('<h3>'+marker.date+'</h3>')
            .addTo(this.map);
        this.markers.push(marker);
        if(this.state.coords.length > 1)
            this.updateRoute();
    }

    addRoute = (coords) => {
        // If a route is already loaded, remove it
        this.clearRoute();
        if(coords.coordinates.length > 1){
            this.map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': coords.type,
                        'coordinates': coords.coordinates
                        // type: 'LineString;
                        // 'coordinates': this.state.coords
                    }
                }
            });
            this.map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                "paint": {
                    "line-color": "#03AA46",
                    "line-width": 5,
                    "line-opacity": 0.5
                }
            });
        }
    }

    deleteLastPoint = () => {
        var marker = this.markers.pop();
        marker.remove();
        // this.markers = this.markers.slice(0, this.markers.length-1);
        this.state.coords = this.state.coords.slice(0, this.state.coords.length-1);
        this.updateRoute();
        var coords = this.state.coords[this.state.coords.length-1];
        this.map.flyTo({
            center: coords,
            essential: true // this animation is considered essential with respect to prefers-reduced-motion
        });
    }

    deleteAll = () => {
        this.state.coords = [];
        this.markers.forEach(marker=>{
            marker.remove();
        });
        this.markers = [];
        if (this.map.getLayer('route')){
            this.map.removeLayer('route')
        }
        if (this.map.getSource('route')) {
            this.map.removeSource('route')
        }
    }

    clearRoute = () => {
        if (this.map.getLayer('route')){
            this.map.removeLayer('route')
        }
        if (this.map.getSource('route')) {
            this.map.removeSource('route')
        }
    }

    updateRoute = () => {
        // Set the profile
        var profile = "driving";
        // Get the coordinates that were drawn on the map
        // var data = this.draw.getAll();
        // var lastFeature = data.features.length - 1;
        // var coords = data.features[lastFeature].geometry.coordinates;
        var coords = this.state.coords;
        // Format the coordinates
        var newCoords = coords.join(';')
        // Set the radius for each coordinate pair to 25 meters
        var radius = [];
        coords.forEach(element => {
          radius.push(25);
        });
        this.getMatch(newCoords, radius, profile);
    }

    getMatch = (coordinates, radius, profile) => {
        // Separate the radiuses with semicolons
        var radiuses = radius.join(';')
        // Create the query
        var query = 'https://api.mapbox.com/matching/v5/mapbox/' + profile + '/' + coordinates + '?geometries=geojson&radiuses=' + radiuses + '&steps=true&access_token=' + mapboxgl.accessToken;
        console.log(query)
        var request = $.ajax({
          method: 'GET',
          url: query
        });
        
        request.done((data)=> {
            try{
            // Get the coordinates from the response
            var coords = data.matchings[0].geometry;
            // Draw the route on the map
            this.addRoute(coords);
          } catch(err) {
            var coords = {type: 'LineString', coordinates: this.state.coords};
            this.addRoute(coords);
          }
        });
        request.fail((error)=>{
            var coords = {type: 'LineString', coordinates: this.state.coords};
            this.addRoute(coords);
        });
    }

    render() {

        return (
            <div id="main">
                
                <div className="center">
                    <section id="content">
                        <div className='sidebarStyle'>
                            <div>Longitude: {this.state.lng} | Latitude: {this.state.lat} | Zoom: {this.state.zoom}</div>
                        </div>
                        <div id='map' ref={el => this.mapContainer = el} className='mapContainer' />
                    </section>
                    <Sidebar addMarker = {this.newMarker} 
                            deleteLastPoint = {this.deleteLastPoint} 
                            reset = {this.deleteAll}
                            lat={this.state.lat} lon={this.state.lng} />

                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default Home;