import React, { Component, Fragment } from "react";
// import { Redirect, Link } from "react-router-dom";

class Sidebar extends Component {

    lonRef = React.createRef();
    latRef = React.createRef();

    // marker = this.props.marker;

    
    state = {
        search: "",
        marker: {
            lon: "",
            lat: ""
        },
        redirect: false
    };

    redirectSearch = (e) => {
        e.preventDefault();
        var value = this.searchRef.current.value;
        console.log(value);
        if(value !== "")
        this.setState({
            search: this.searchRef.current.value,
            redirect: true
        });
    };

    addMarker = (e) => {
        // this.props.add
        var newMarker = {
            lng: this.lonRef.current.value,
            lat: this.latRef.current.value,
            date: new Date()
        }
        this.props.addMarker(newMarker);
    }

    deleteLast = (e) => {
        e.preventDefault();
        this.props.deleteLastPoint();
    }


    reset = (e) => {
        e.preventDefault();
        this.props.reset();
    }
    

    

    render() {
        // if (this.state.redirect) {
        //     return (
        //         <Redirect to={"/redirect/" + this.state.search} />
        //     );
        // }
        return (

            <Fragment>
                <aside id="sidebar">
                    {/* <div id="nav-blog" className="sidebar-item">
                        <h3>Puedes hacer esto</h3>
                        <a href="/article" className="btn btn-success">Crear articulo</a>
                        <Link to="/blog/new" className="btn btn-success">Crear articulo</Link>
                    </div> */}
                    <div id="search" className="sidebar-item">
                        {/* <h3>Buscador</h3> */}
                        {/* <p>Encuentra el articulo que buscas</p> */}
                        {/* <form onSubmit="" action=""> */}
                            <label  htmlfor="lon">Longitud:</label>
                            <input type="text" name="lon" id="lon" value={this.props.lon} ref={this.lonRef} />
                            <label  htmlfor="lat">Latitud:</label>
                            <input type="text" name="lat" id="lat" value={this.props.lat} ref={this.latRef} />
                            {/* <input className="btn" on type="submit" name="submit" value="Agregar punto" id="" /> */}
                            <button className="btn" onClick={this.addMarker}>
                            Agregar punto
                            </button>
                            <button className="btn" onClick={this.deleteLast}>
                            Eliminar último
                            </button>
                            <button className="btn" onClick={this.reset}>
                            Limpiar mapa
                            </button>
                        {/* </form> */}
                    </div>
                </aside>
                <div className="clearfix"></div>
            </Fragment>
        );
    }
}

export default Sidebar;