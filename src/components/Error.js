import React from "react";
import Sidebar from "./Sidebar";


const Error404 = function () {
    return (
        <div className="center">
            <section id="content">
                <h2 className="subheader">Página no encontrada</h2>
                <p>La ṕagina no existe</p>
            </section>

            <Sidebar />

            <div className="clearfix"></div>
        </div>
    );
}

export default Error404;