import React, { Component, Fragment } from "react";

class Slider extends Component {
    render() {
        return (
            <Fragment>
                <div id="slider" className={this.props.size}>
                    {/* Se pasan variables desde el componente y se acceden mediante
                    this.props */}
                    <h1>{this.props.title}</h1>
                    {
                        this.props.btn &&
                        <a href="blog.html" className="btn-white">{this.props.btn}</a>
                    }
                </div>
            </Fragment>
        );
    }
}

export default Slider;